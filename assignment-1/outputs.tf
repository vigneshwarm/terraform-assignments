output "ec2_ami" {
  value       = data.aws_ami.ubuntu
  description = "ami for the ec2"
}

output "alb_name" {
  value       = aws_alb.web_alb.dns_name
  description = "public dns for the ALB"
}

output "rds_endpoint" {
  value       = aws_db_instance.rds_web.endpoint
  description = "private RDS endpoint"
}
