resource "aws_db_instance" "rds_web" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t3.micro"
  db_name                = "mydb"
  username               = var.rds_user
  password               = var.rds_password
  parameter_group_name   = "default.mysql5.7"
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.db_subnet.name
  vpc_security_group_ids = [aws_security_group.rds_sg.id]

  provisioner "local-exec" {
    //command = "echo ${var.rds_password} > scripts/password.txt"
    command = <<EOF
     echo ${var.rds_password} > scripts/password.txt
     echo ${var.rds_user} > scripts/username.txt
     echo ${self.endpoint} > scripts/host.txt
     EOF
  }

  tags = {
    "name" = "mysql_rds"
  }
}
