variable "user_data" {
  default     = "scripts/install.sh"
  description = "user data for ec2 config for the apache and php installation"
}

variable "my_ip" {
  default     = "122.187.88.146/32"
  description = "cidr block for vpc/subnets/sg"
}

variable "rds_user" {
  default     = "root"
  description = "username for rds"
}

variable "rds_password" {
  default     = "password"
  description = "password for the rds user"
}

variable "key_pair_name" {
  default     = "tf-key"
  description = "key pair for ssh-ing into the ec2"
}

variable "all_access_cidr_blocks" {
  default     = ["0.0.0.0/0"]
  description = "cidr for engress"
}
