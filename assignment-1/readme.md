## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.29.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.29.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_alb.web_alb](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/alb) | resource |
| [aws_alb_listener.listener_http](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/alb_listener) | resource |
| [aws_alb_target_group.alb_group](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/alb_target_group) | resource |
| [aws_db_instance.rds_web](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/db_instance) | resource |
| [aws_db_subnet_group.db_subnet](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/db_subnet_group) | resource |
| [aws_instance.web](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/instance) | resource |
| [aws_internet_gateway.main_igw](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/internet_gateway) | resource |
| [aws_lb_target_group_attachment.alb_web_attachment](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/lb_target_group_attachment) | resource |
| [aws_route_table.private_rt](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/route_table) | resource |
| [aws_route_table.public_rt](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/route_table) | resource |
| [aws_route_table_association.main_private_1_rt_association](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/route_table_association) | resource |
| [aws_route_table_association.main_private_2_rt_association](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/route_table_association) | resource |
| [aws_route_table_association.main_public_1_rt_association](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/route_table_association) | resource |
| [aws_route_table_association.main_public_2_rt_association](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/route_table_association) | resource |
| [aws_security_group.alb](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/security_group) | resource |
| [aws_security_group.rds_sg](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/security_group) | resource |
| [aws_security_group.web_sg](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/security_group) | resource |
| [aws_subnet.private_1](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/subnet) | resource |
| [aws_subnet.private_2](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/subnet) | resource |
| [aws_subnet.public_1](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/subnet) | resource |
| [aws_subnet.public_2](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/subnet) | resource |
| [aws_vpc.main](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/resources/vpc) | resource |
| [aws_ami.ubuntu](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/data-sources/ami) | data source |
| [aws_instances.alb_instances](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/data-sources/instances) | data source |
| [aws_subnets.private](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/data-sources/subnets) | data source |
| [aws_subnets.public](https://registry.terraform.io/providers/hashicorp/aws/4.29.0/docs/data-sources/subnets) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allowed_cidr_blocks"></a> [allowed\_cidr\_blocks](#input\_allowed\_cidr\_blocks) | cidr block for vpc/subnets/sg | `list` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_key_pair_name"></a> [key\_pair\_name](#input\_key\_pair\_name) | key pair for ssh-ing into the ec2 | `string` | `"tf-key"` | no |
| <a name="input_rds_password"></a> [rds\_password](#input\_rds\_password) | password for the rds user | `string` | `"password"` | no |
| <a name="input_rds_user"></a> [rds\_user](#input\_rds\_user) | username for rds | `string` | `"root"` | no |
| <a name="input_user_data"></a> [user\_data](#input\_user\_data) | user data for ec2 config for the apache and php installation | `string` | `"scripts/install.sh"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_alb_name"></a> [alb\_name](#output\_alb\_name) | public dns for the ALB |
| <a name="output_ec2_ami"></a> [ec2\_ami](#output\_ec2\_ami) | ami for the ec2 |
| <a name="output_rds-endpoint"></a> [rds-endpoint](#output\_rds-endpoint) | private RDS endpoint |
