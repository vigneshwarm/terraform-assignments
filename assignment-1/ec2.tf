resource "aws_instance" "web" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t3.micro"
  subnet_id                   = aws_subnet.public_1.id
  key_name                    = var.key_pair_name
  user_data                   = file(var.user_data)
  vpc_security_group_ids      = [aws_security_group.web_sg.id]
  associate_public_ip_address = true
  count                       = 2
  depends_on                  = [aws_db_instance.rds_web]

  tags = {
    Name = "web-${count.index}"
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("keys/tf-key.pem")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "scripts/password.txt"
    destination = "/home/ubuntu/password.txt"
  }
  provisioner "file" {
    source      = "scripts/username.txt"
    destination = "/home/ubuntu/username.txt"
  }
  provisioner "file" {
    source      = "scripts/host.txt"
    destination = "/home/ubuntu/host.txt"
  }
}
