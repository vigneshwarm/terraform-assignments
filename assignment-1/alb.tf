resource "aws_alb" "web_alb" {
  name            = "web-alb-main"
  security_groups = [aws_security_group.alb.id]
  subnets         = [aws_subnet.public_1.id, aws_subnet.public_2.id]
  depends_on = [
    aws_instance.web
  ]
  tags = {
    Name = "web_alb"
  }
}

resource "aws_alb_target_group" "alb_group" {
  name        = "web-alb-main-target"
  port        = 80
  target_type = "instance"
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id

  # Alter the destination of the health check to be the index page.
  health_check {
    path = "/"
    port = 80
  }
}

resource "aws_alb_listener" "listener_http" {
  load_balancer_arn = aws_alb.web_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.alb_group.arn
    type             = "forward"
  }
}

resource "aws_lb_target_group_attachment" "alb_web_attachment" {
  for_each         = toset(aws_instance.web.*.id)
  target_group_arn = aws_alb_target_group.alb_group.arn
  target_id        = each.value
  port             = 80
}

