# Readme

## Assignment - 1

Create the terraform template to do the following:

1. Create a VPC with 4 subnets (2 private and 2 public ) in different availability zones. 
2. Create two EC2 instances in public subnet with Apache2
3. Create an application load balancer and attach the above created ec2 instance to it
4. Create an RDS MySQL instance running on 3306 port
5. Set right security group rules to access the above application over the internet via 80 port and have connectivity to the mysql instance with 3306 port

## Assignment - 2

Make updates to the Assignment 4 terraform template with the following additional requirements:

1. Create a separate module to provision vpc and subnets with environment tag value as "training"
2. Create separate module to provision an EC2 instance with possible instance name, type, ami, count, tag values etc., as module inputs
3. Use data resource to get the VPC and subnet details based on the tag name "environment=training"
4. Use provisioner to deploy the mysql-connection.php to the Apache2 instance /var/www/html/ with the provisioned RDS instance as part of this template.
